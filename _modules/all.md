---
---

Form 5 
: [KSSM Chemistry](https://youtube.com/playlist?list=PLtRtVdOWr_N_tP7SFi-scXleYw8rbX0iL)
  : 学姐
: 1. [Chapter 2 Carbon Compounds: 2.1 Types of Carbon Compounds](https://youtu.be/QSUggwZq6r8)
  1. [Chapter 2 Carbon Compounds: 2.2 Homologous Series](https://youtu.be/CgIeqEQmemA)
  1. [Chapter 2 Carbon Compounds: 2.3 Chemical Properties](https://youtu.be/ShZ6BMPcsjY)
