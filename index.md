---
layout: minimal
title: 课本导读
nav_exclude: true
seo:
  type: Course
  name: SPM
---

# {{ site.tagline }}
{: .mb-2 }
{{ site.description }}
{: .fs-6 .fw-300 }

{% assign instructors = site.staffers | where: 'role', 'Instructor' %}
{% for staffer in instructors %}
{{ staffer }}
{% endfor %}

教育应该是免费的，采用中文导读KPM讲义。
{% for module in site.modules %}
{{ module }}
{% endfor %}
